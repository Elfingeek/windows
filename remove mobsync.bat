@echo off
echo.	remove microsoft sync center
taskkill /im mobsync.exe
takeown /f "C:\Windows\System32\mobsync.exe" /a
icacls "C:\Windows\System32\mobsync.exe" /grant:r Administrators:F /c
del "C:\Windows\System32\mobsync.exe"
