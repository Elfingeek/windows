@echo off
echo cloudflare
del /q/s "%ProgramData%\Cloudflare\*.txt"
del /q/s "%LocalAppData%\Cloudflare\*.log"
echo Typora
del /q/s "%AppData%\Typora\Cache"
del /q/s "%AppData%\Typora\Code Cache"
del /q/s "%AppData%\Typora\Crashpad"
del /q/s "%AppData%\Typora\DawnCache"
del /q/s "%AppData%\Typora\GPUCache"
del /q/s "%AppData%\Typora\IndexedDB"
echo Xmind
rd /q/s "%LocalAppData%\xmind-updater"
del /q/s "%AppData%\Xmind\Electron v3\Cache"
del /q/s "%AppData%\Xmind\Electron v3\Code Cache"
del /q/s "%AppData%\Xmind\Electron v3\Crashpad"
del /q/s "%AppData%\Xmind\Electron v3\DawnCache"
del /q/s "%AppData%\Xmind\Electron v3\GPUCache"
del /q/s "%AppData%\Xmind\Electron v3\IndexedDB"
echo WPS
rd /q/s "%AppData%\Kingsoft\office6\log"
del /q/s "%AppData%\Kingsoft\office6\cache"
del /q/s "%AppData%\Kingsoft\office6\backup"
del /q/s "%AppData%\Kingsoft\office6\OfficeSpace"
echo zlibrary
rd /q/s "%LocalAppData%\z-library-updater"
echo Edge
rd /q/s "%ProgramData%\Microsoft\EdgeUpdate"
rd /q/s "%ProgramFiles(x86)%\Microsoft\EdgeUpdate\Download"
rd /q/s "%ProgramFiles(x86)%\Microsoft\EdgeUpdate\Install"
rd /q/s "%ProgramFiles(x86)%\Microsoft\EdgeUpdate\Offline"
rd /q/s "%ProgramFiles(x86)%\Microsoft\Edge\Application\SetupMetrics"
rd /q/s "%ProgramFiles(x86)%\Microsoft\EdgeCore"
del /q/s "%ProgramFiles(x86)%\Microsoft\Temp"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\BrowserMetrics"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\Crashpad"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\GrShaderCache"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\ShaderCache"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\Default\Cache"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\Default\Code Cache"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\Default\GPUCache"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\Default\IndexedDB"
del /q/s "%LocalAppData%\Microsoft\Edge\User Data\Default\Service Worker"
pause