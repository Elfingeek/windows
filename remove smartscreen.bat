@echo off
echo.	remove windows defender smartscreen
taskkill /im smartscreen.exe
takeown /f "C:\Windows\System32\smartscreen.exe" /a
icacls "C:\Windows\System32\smartscreen.exe" /grant:r Administrators:F /c
del "C:\Windows\System32\smartscreen.exe"
