@echo off
echo IconCache
taskkill /f /im explorer.exe
del /q "%LocalAppData%\iconcache.db"
del /q "%LocalAppData%\Microsoft\Windows\Explorer\iconcache*.db"
start explorer
echo Windows Logs
net stop EventLog
del /q/s "%WinDir%\Logs\"
del /q/s "%WinDir%\System32\LogFiles\"
del /q/s "%WinDir%\System32\WDI\LogFiles\"
del /q/s "%WinDir%\System32\winevt\Logs\"
del /q/s "%WinDir%\SysWOW64\LogFiles\"
del /q/s "%WinDir%\Performance\WinSAT\"
net start EventLog
echo Windows Cache
del /q/s "%ProgramFiles(x86)%\Microsoft\temp\"
del /q/s "%LocalAppData%\Temp\"
del /q/s "%WinDir%\assembly\temp\"
del /q/s "%WinDir%\assembly\NativeImages_v4.0.30319_32\"
del /q/s "%WinDir%\assembly\NativeImages_v4.0.30319_64\"
del /q/s "%ProgramData%\Microsoft\Windows\caches\"
del /q/s "%LocalAppData%\Microsoft\Windows\caches\"
del /q/s "%LocalLow%\Microsoft\CryptnetUrlCache\"
echo Windows Prefetch
del /q/s "%WinDir%\Prefetch\"
echo NVIDIA
del /q/s "%ProgramFiles%\NVIDIA Corporation\Installer2\"
