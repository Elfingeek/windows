@echo off
echo AutoRun
reg delete "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "SecurityHealth" /f
reg delete "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run" /v "RtkAudUService" /f
echo SendTo
del /q "%AppData%\Microsoft\Windows\SendTo\Bluetooth device.LNK"
del /q "%AppData%\Microsoft\Windows\SendTo\Compressed (zipped) folder.ZFSendToTarget"
del /q "%AppData%\Microsoft\Windows\SendTo\Documents.mydocs"
del /q "%AppData%\Microsoft\Windows\SendTo\Mail recipient.MAPIMail"
del /q "C:\Users\Default\AppData\Roaming\Microsoft\Windows\SendTo\Bluetooth device.LNK"
del /q "C:\Users\Default\AppData\Roaming\Microsoft\Windows\SendTo\Compressed (zipped) folder.ZFSendToTarget"
del /q "C:\Users\Default\AppData\Roaming\Microsoft\Windows\SendTo\Documents.mydocs"
del /q "C:\Users\Default\AppData\Roaming\Microsoft\Windows\SendTo\Mail recipient.MAPIMail"
pause